import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './iPhone12ProMax1.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class iPhone12ProMax2 extends StatelessWidget {
  iPhone12ProMax2({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 67.0, start: 115.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: -3.0, end: 3.0),
            Pin(size: 92.0, start: -11.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 116.0, middle: 0.5),
            Pin(size: 42.0, start: 128.0),
            child: Text(
              'Éclairage',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 107.0, start: 40.0),
            Pin(size: 42.0, middle: 0.2376),
            child: Text(
              'Intensité',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xff000000),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 116.0, middle: 0.5),
            Pin(size: 42.0, start: 128.0),
            child: Text(
              'Éclairage',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 166.0, end: 51.0),
            Pin(size: 1.0, middle: 0.2508),
            child: SvgPicture.string(
              _svg_b4fbth,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 17.0, middle: 0.5766),
            Pin(size: 19.0, middle: 0.2448),
            child: Container(
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 137.0, start: 40.0),
            Pin(size: 42.0, middle: 0.3167),
            child: Text(
              'Luminosité',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xff000000),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 166.0, end: 59.0),
            Pin(size: 1.0, middle: 0.3254),
            child: SvgPicture.string(
              _svg_dojlcf,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 17.0, middle: 0.6959),
            Pin(size: 19.0, middle: 0.3208),
            child: Container(
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 67.0, middle: 0.4075),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 116.0, middle: 0.5),
            Pin(size: 42.0, middle: 0.4106),
            child: Text(
              'Humidité',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 107.0, start: 40.0),
            Pin(size: 42.0, middle: 0.5034),
            child: Text(
              'Intensité',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xff000000),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 166.0, end: 59.0),
            Pin(size: 1.0, middle: 0.5016),
            child: SvgPicture.string(
              _svg_6vee5p,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 17.0, middle: 0.6545),
            Pin(size: 19.0, middle: 0.5006),
            child: Container(
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 67.0, middle: 0.6088),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 159.0, middle: 0.5019),
            Pin(size: 42.0, middle: 0.6063),
            child: Text(
              'Température',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 98.0, start: 37.0),
            Pin(size: 42.0, middle: 0.6991),
            child: Text(
              'Chaleur',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xff000000),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 166.0, end: 59.0),
            Pin(size: 1.0, middle: 0.6908),
            child: SvgPicture.string(
              _svg_tgjxdt,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 17.0, middle: 0.6204),
            Pin(size: 19.0, middle: 0.6913),
            child: Container(
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 158.0, middle: 0.5),
            Pin(size: 32.0, end: 17.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.SlideDown,
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => iPhone12ProMax1(),
                ),
              ],
              child: Text(
                'EPSI 2020 - 2021',
                style: TextStyle(
                  fontFamily: 'Songti SC',
                  fontSize: 23,
                  color: const Color(0xff000000),
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 177.0, middle: 0.502),
            Pin(size: 1.0, end: 59.0),
            child: SvgPicture.string(
              _svg_jmtvzt,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 32.0, start: 32.0),
            Pin(size: 31.0, start: 22.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.SlideDown,
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => iPhone12ProMax1(),
                ),
              ],
              child: SvgPicture.string(
                _svg_svxo2t,
                allowDrawingOutsideViewBox: true,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 278.0, end: 61.0),
            Pin(size: 42.0, start: 16.0),
            child: Text(
              'Terrarium Application',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_b4fbth =
    '<svg viewBox="211.0 232.0 166.0 1.0" ><path transform="translate(211.0, 232.0)" d="M 0 0 L 166 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_dojlcf =
    '<svg viewBox="203.0 301.0 166.0 1.0" ><path transform="translate(203.0, 301.0)" d="M 0 0 L 166 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_6vee5p =
    '<svg viewBox="203.0 464.0 166.0 1.0" ><path transform="translate(203.0, 464.0)" d="M 0 0 L 166 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_tgjxdt =
    '<svg viewBox="203.0 639.0 166.0 1.0" ><path transform="translate(203.0, 639.0)" d="M 0 0 L 166 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_jmtvzt =
    '<svg viewBox="126.0 866.0 177.0 1.0" ><path transform="translate(126.0, 866.0)" d="M 0 0 L 177 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_svxo2t =
    '<svg viewBox="32.0 22.0 32.0 31.0" ><path transform="translate(32.0, 22.0)" d="M 15.99999809265137 0 L 31.99999809265137 31 L 0 31 Z" fill="#ffffff" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
