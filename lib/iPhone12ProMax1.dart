import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './iPhone12ProMax2.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class iPhone12ProMax1 extends StatelessWidget {
  iPhone12ProMax1({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Pinned.fromPins(
            Pin(start: 0.0, end: 0.0),
            Pin(size: 92.0, start: -11.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 278.0, middle: 0.5),
            Pin(size: 42.0, start: 16.0),
            child: Text(
              'Terrarium Application',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 0.0, end: -4.3),
            Pin(size: 158.0, start: 80.0),
            child:
                // Adobe XD layer: 'Atelier-Terrarium-e…' (shape)
                Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/Terra.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 67.0, middle: 0.2887),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 154.0, middle: 0.5),
            Pin(size: 42.0, middle: 0.2952),
            child: Text(
              'Présentation',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 16.0, end: 16.0),
            Pin(size: 116.0, middle: 0.4012),
            child: Text(
              'Terrarium Application est une application \nmobile de gestion d’un terrarium conçue pour \nle MyDil EPSI Montpellier 2021\n',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 21,
                color: const Color(0xff000000),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 67.0, middle: 0.5006),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff000000),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.0, end: 17.0),
            Pin(size: 313.0, end: 119.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: const Color(0xff484848),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 279.0, middle: 0.5168),
            Pin(size: 42.0, middle: 0.5),
            child: Text(
              'Données du Terrarium',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 301.0, start: 52.0),
            Pin(size: 42.0, middle: 0.5905),
            child: Text(
              'Éclairage                 20 lx',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 301.0, start: 50.0),
            Pin(size: 1.0, middle: 0.6454),
            child: SvgPicture.string(
              _svg_8hp7mw,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 51.0, end: 51.0),
            Pin(size: 42.0, middle: 0.7127),
            child: Text(
              'Température           30 °C',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 301.0, start: 52.0),
            Pin(size: 1.0, middle: 0.7622),
            child: SvgPicture.string(
              _svg_x9v06c,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 311.0, start: 50.0),
            Pin(size: 42.0, middle: 0.8348),
            child: Text(
              'Humidité                 0.5 %',
              style: TextStyle(
                fontFamily: 'Songti SC',
                fontSize: 30,
                color: const Color(0xffffffff),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 177.0, middle: 0.502),
            Pin(size: 1.0, end: 60.0),
            child: SvgPicture.string(
              _svg_sjz7av,
              allowDrawingOutsideViewBox: true,
              fit: BoxFit.fill,
            ),
          ),
          Pinned.fromPins(
            Pin(size: 158.0, middle: 0.5),
            Pin(size: 32.0, end: 19.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.SlideUp,
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => iPhone12ProMax2(),
                ),
              ],
              child: Text(
                'EPSI 2020 - 2021',
                style: TextStyle(
                  fontFamily: 'Songti SC',
                  fontSize: 23,
                  color: const Color(0xff000000),
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(size: 32.0, start: 34.0),
            Pin(size: 31.0, start: 22.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.SlideUp,
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => iPhone12ProMax2(),
                ),
              ],
              child: SvgPicture.string(
                _svg_cfk1x1,
                allowDrawingOutsideViewBox: true,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_8hp7mw =
    '<svg viewBox="50.0 597.0 301.0 1.0" ><path transform="translate(50.0, 597.0)" d="M 0 0 L 301 0" fill="none" stroke="#9f9f9f" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_x9v06c =
    '<svg viewBox="52.0 705.0 301.0 1.0" ><path transform="translate(52.0, 705.0)" d="M 0 0 L 301 0" fill="none" stroke="#9f9f9f" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_sjz7av =
    '<svg viewBox="126.0 865.0 177.0 1.0" ><path transform="translate(126.0, 865.0)" d="M 0 0 L 177 0" fill="none" stroke="#000000" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_cfk1x1 =
    '<svg viewBox="34.0 22.0 32.0 31.0" ><path transform="translate(34.0, 22.0)" d="M 16 0 L 32 31 L 0 31 Z" fill="#ffffff" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
